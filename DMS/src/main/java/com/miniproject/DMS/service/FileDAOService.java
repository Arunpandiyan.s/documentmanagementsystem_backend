package com.miniproject.DMS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miniproject.DMS.entity.FileDAO;
import com.miniproject.DMS.repository.FileDAORepository;

import jakarta.persistence.EntityManager;

@Service
public class FileDAOService {

	private EntityManager entityManager;

	@Autowired
	public FileDAOService(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Autowired
	private FileDAORepository fileDAORepository;

	public FileDAO create(FileDAO fileDAO) {
		return fileDAORepository.save(fileDAO);
	}

	public List<FileDAO> getall() {
		return fileDAORepository.findAll();
	}

	public List<FileDAO> getbyUsername(String username) {

		return fileDAORepository.findByCreatedby(username);

	}

}

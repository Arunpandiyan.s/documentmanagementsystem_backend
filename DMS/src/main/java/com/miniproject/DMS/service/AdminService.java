package com.miniproject.DMS.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miniproject.DMS.entity.Admin;
import com.miniproject.DMS.repository.AdminRepository;

@Service
public class AdminService {
	
	@Autowired
	private AdminRepository adminRepository;

	public Admin login( Admin adminData){
		Admin admin= adminRepository.findByAdminname(adminData.getAdminname());
		return admin;
	}
}

package com.miniproject.DMS.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.miniproject.DMS.entity.User;
import com.miniproject.DMS.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User createuser(User user) {
		return userRepository.save(user);
	}

	public User login(User userData) {
		User user = userRepository.findByUsername(userData.getUsername());
		return user;
	}

	public List<User> getall() {
		return userRepository.findAll();
	}

}

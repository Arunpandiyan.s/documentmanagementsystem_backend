package com.miniproject.DMS.service;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.miniproject.DMS.entity.File;
import com.miniproject.DMS.entity.FileDAO;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;

@Service
public class FileService {

	@Autowired
	private GridFsTemplate template;

	@Autowired
	private GridFsOperations operations;

	@Autowired
	private FileDAOService fileDAOService;

	public String addFile(MultipartFile file,String username) throws IOException {

		DBObject metadata = new BasicDBObject();
		metadata.put("fileSize", file.getSize());

		FileDAO fileDAO = new FileDAO();
		fileDAO.setFilename(file.getOriginalFilename());
		fileDAO.setFileSize(file.getSize());
		fileDAO.setCreatedby(username);
		fileDAOService.create(fileDAO);

		Object fileID = template.store(file.getInputStream(), file.getOriginalFilename(), file.getContentType(),
				metadata);
		System.out.println(fileID.toString());

		return fileID.toString();
	}

	public File downloadFile(String filename) throws IOException {

		GridFSFile gridFSFile = template.findOne(new Query(Criteria.where("filename").is(filename)));

		File file = new File();

		if (gridFSFile != null && gridFSFile.getMetadata() != null) {
			file.setFilename(gridFSFile.getFilename());

			file.setFileType(gridFSFile.getMetadata().get("_contentType").toString());

			file.setFileSize(gridFSFile.getMetadata().get("fileSize").toString());

			file.setFile(IOUtils.toByteArray(operations.getResource(gridFSFile).getInputStream()));
		}

		return file;
	}

}

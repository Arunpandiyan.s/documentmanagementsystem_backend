package com.miniproject.DMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.DMS.entity.Admin;
import com.miniproject.DMS.service.AdminService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
public class AdminController {
	
	
	
	@Autowired
	private AdminService adminService;
	

	
	@PostMapping("/admin/login")
	public ResponseEntity<Admin> loginAdmin(@RequestBody Admin adminData){
		Admin admin= adminService.login(adminData);
		System.out.println(admin);
		if(admin.getPassword().equals(adminData.getPassword()))
			return ResponseEntity.ok(admin);
		return (ResponseEntity<Admin>) ResponseEntity.internalServerError();
	}
	


}

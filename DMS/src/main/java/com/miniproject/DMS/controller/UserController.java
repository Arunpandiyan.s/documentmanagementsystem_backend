package com.miniproject.DMS.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.DMS.entity.FileDAO;
import com.miniproject.DMS.entity.User;
import com.miniproject.DMS.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/createuser")
	public User create(@RequestBody User user) {
		return userService.createuser(user);
	}
	
	@PostMapping("/user/login")
	public ResponseEntity<User> loginAdmin(@RequestBody User userData){
		User user= userService.login(userData);
		System.out.println(user);
		if(user.getPassword().equals(userData.getPassword()))
			return ResponseEntity.ok(user);
		return (ResponseEntity<User>) ResponseEntity.internalServerError(); 
	}
	@GetMapping("/user/all")
    public List<User> getAllUsers(){
    	return userService.getall();
    }

}

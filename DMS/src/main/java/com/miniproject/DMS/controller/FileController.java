package com.miniproject.DMS.controller;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.miniproject.DMS.entity.File;
import com.miniproject.DMS.entity.FileDAO;
import com.miniproject.DMS.service.EmailSenderService;
import com.miniproject.DMS.service.FileDAOService;
import com.miniproject.DMS.service.FileService;

@RestController
@CrossOrigin("*")
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;
    
    @Autowired
    private FileDAOService fileDAOService;
    
    @Autowired
	private EmailSenderService senderService;

    @PostMapping("/upload/{username}/{email}")
    public ResponseEntity<?> upload(@RequestParam("file")MultipartFile file,@PathVariable("username") String createdby,@PathVariable("email") String email) throws IOException {
		senderService.sendEmail(email, "Your action on file","File uploaded successfully");
        return new ResponseEntity<>(fileService.addFile(file,createdby), HttpStatus.OK);
    }

    @GetMapping("/download/{filename}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable String filename) throws IOException {
        File file = fileService.downloadFile(filename);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(file.getFileType() ))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(new ByteArrayResource(file.getFile()));
    }
    
    @GetMapping("/view/files")
    public List<FileDAO> view(){
    	return fileDAOService.getall();
    }
    @GetMapping("/view/files/{createdby}")
    public List<FileDAO> viw(@PathVariable("createdby") String username){
    	return fileDAOService.getbyUsername(username);
    }

   
}

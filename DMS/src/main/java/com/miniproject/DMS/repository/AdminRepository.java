package com.miniproject.DMS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.miniproject.DMS.entity.Admin;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer>{


	Admin findByAdminname(String adminname);





}

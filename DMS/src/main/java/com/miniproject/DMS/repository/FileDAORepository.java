package com.miniproject.DMS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.miniproject.DMS.entity.FileDAO;

@Repository
public interface FileDAORepository extends JpaRepository<FileDAO, Integer> {

	List<FileDAO> findByCreatedby(String username);

}
